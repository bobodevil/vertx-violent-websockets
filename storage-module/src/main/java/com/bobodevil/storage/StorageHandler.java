package com.bobodevil.storage;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

public class StorageHandler {
	private Vertx vertx;
	private Container container;
	
	public StorageHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
		
		vertx.eventBus().registerHandler("com.bobodevil.module.storage.chatlog", logChatHandler());
	}
	
	
	public Handler<Message<JsonObject>> logChatHandler() {
		return new Handler<Message<JsonObject>>() {
			
			public void handle(final Message<JsonObject> message) {
				final JsonObject query = new JsonObject();
				query.putString("action", "save");
				query.putString("collection", "messages");
				query.putObject("document", message.body());
				
			    container.logger().info("call()");
			    final Integer eventTimeout = 2000; //100ms;
			    vertx.eventBus().sendWithTimeout(
						"com.bobodevil.chatlog" , query, eventTimeout ,new Handler<AsyncResult<Message<JsonObject>>>() {
					@Override
					public void handle(AsyncResult<Message<JsonObject>> asyncMessage) {
						message.reply();
						//EMPTY
					}
				});
			}
		};
	}
		
}
