package com.bobodevil.storage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class StorageVerticle extends Verticle {
	public void start() {
		JsonObject appConfig = container.config();
		
		if (appConfig.getObject("chat-storage-persistor", null) == null) {
			try {
				byte[] encoded = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("config.json").toURI()));
				container.logger().info(new String(encoded));
				appConfig = new JsonObject(new String(encoded));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	  
		container.deployModule("io.vertx~mod-mongo-persistor~2.1.0", appConfig.getObject("chat-storage-persistor"));
		
		StorageHandler sh = new StorageHandler(vertx, container);
		
		container.logger().info("Storage Container started");
	}
}