package com.bobodevil.web.handler;


import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.vertx.java.core.Handler;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.ServerWebSocket;
import org.vertx.java.core.json.JsonArray;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class WebSocketHandler {
	
	private Vertx vertx;
	private Container container;
	
	
	public WebSocketHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
	}
	
	public HttpServer configRoutes(HttpServer server) {
		final Pattern chatUrlPattern = Pattern.compile("/chat/(\\w+)");
		
		
		server.websocketHandler(new Handler<ServerWebSocket>() {
			@Override
			public void handle(final ServerWebSocket ws) {
				final Matcher m = chatUrlPattern.matcher(ws.path());
				if (!m.matches()) {
					ws.reject();
					return;
				}
		 
				final String chatRoom = m.group(1);
				final String id = ws.textHandlerID();
				container.logger().info("registering new connection with id: " + id + " for chat-room: " + chatRoom);
				vertx.sharedData().getSet("chat.room." + chatRoom).add(id);
		 
				ws.closeHandler(new Handler<Void>() {
					@Override
					public void handle(final Void event) {
						container.logger().info("un-registering connection with id: " + id + " from chat-room: " + chatRoom);
						vertx.sharedData().getSet("chat.room." + chatRoom).remove(id);
					}
				});
		 
				ws.dataHandler(new Handler<Buffer>() {
					@Override
					public void handle(final Buffer data) {
		 
						ObjectMapper m = new ObjectMapper();
						try {
							JsonNode rootNode = m.readTree(data.toString());
							((ObjectNode) rootNode).put("received", new Date().toString());
							String jsonOutput = m.writeValueAsString(rootNode);
							container.logger().info("json generated: " + jsonOutput);
							
							//Logging
							vertx.eventBus().send("com.bobodevil.module.storage.chatlog", new JsonObject(jsonOutput));
							
							
							for (Object chatter : vertx.sharedData().getSet("chat.room." + chatRoom)) {
								vertx.eventBus().send((String) chatter, jsonOutput);
							}
						} catch (IOException e) {
							ws.reject();
						}
					}
				});
			}
		});
		
		return server;
	}
	
	
}
