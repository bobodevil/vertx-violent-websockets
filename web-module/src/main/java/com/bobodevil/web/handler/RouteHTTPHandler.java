package com.bobodevil.web.handler;

import io.vertx.rxcore.java.eventbus.RxEventBus;
import io.vertx.rxcore.java.eventbus.RxMessage;

import java.io.File;
import java.util.Map;

import org.vertx.java.core.Handler;
import org.vertx.java.core.MultiMap;
import org.vertx.java.core.Vertx;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Container;

import rx.Observable;
import rx.util.functions.Action1;

import com.bobodevil.utils.ConvertMapFromMultiMap;
import com.bobodevil.web.utils.RequestUtils;


public class RouteHTTPHandler {
	
	private Vertx vertx;
	private Container container;
	
	
	public RouteHTTPHandler(Vertx vertx, Container container) {
		this.container = container;
		this.vertx = vertx;
	}
	
	public HttpServer configRoutes(HttpServer server) {
		final JsonObject appConfig = container.config();
		
		final EventBus eb = vertx.eventBus();
		
		RouteMatcher matcher = new RouteMatcher().get("/", new Handler<HttpServerRequest>() {
			@Override
			public void handle(final HttpServerRequest request) {
				request.response().sendFile("web/chat.html");
			}
		}).get(".*\\.(css|js)$", new Handler<HttpServerRequest>() {
			@Override
			public void handle(final HttpServerRequest request) {
				request.response().sendFile("web/" + new File(request.path()));
			}
		});
		
		server.requestHandler(matcher);
		
		return server;
	}
	
	
}
