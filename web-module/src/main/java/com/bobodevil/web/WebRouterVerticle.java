package com.bobodevil.web;

import org.vertx.java.busmods.BusModBase;
import org.vertx.java.core.http.HttpServer;
import com.bobodevil.web.handler.RouteHTTPHandler;
import com.bobodevil.web.handler.WebSocketHandler;

public class WebRouterVerticle extends BusModBase {
	
	
	public void start() {
		super.start();
		container.deployModule("com.bobodevil.violent~storage-module~0.1");
		
		
		startHTTPServer();
		startWebSocketServer();
		container.logger().info("WebRouterVerticle started");
	}
	
	
	private void startHTTPServer() {
		HttpServer server = vertx.createHttpServer();
		
		RouteHTTPHandler rh = new RouteHTTPHandler(vertx, container);
		rh.configRoutes(server).listen(8811);
	}
	
	private void startWebSocketServer() {
		HttpServer server = vertx.createHttpServer();
		
		WebSocketHandler wsh = new WebSocketHandler(vertx, container);
		wsh.configRoutes(server).listen(8812);
	}
	
	
}