package com.bobodevil.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.vertx.java.core.MultiMap;

public class ConvertMapFromMultiMap implements IConvertor<Map<String, Object>, MultiMap>{

	@Override
	public Map<String, Object> convert(MultiMap from) {
		Map<String, Object> toMap = new HashMap<>();
		Iterator<Entry<String, String>> it = from.iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			toMap.put(entry.getKey(), entry.getValue()) ;
		}
		return toMap;
	}

}
