package com.bobodevil;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class ContainerVerticle extends Verticle {
	public void start() {
		JsonObject appConfig = container.config();
		
		if (appConfig.getObject("web-module", null) == null) {
			try {
				byte[] encoded = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("config.json").toURI()));
				container.logger().info(new String(encoded));
				appConfig = new JsonObject(new String(encoded));
			} catch (IOException | URISyntaxException e) {
				e.printStackTrace();
			}
		}
	  
	  
		container.deployModule("com.bobodevil.violent~web-module~0.1", appConfig.getObject("web-module"));
	  
		container.logger().info("Container started");
	}
}